$(document).ready(function () {
    var swiper = new Swiper('.friends-slider', {
        slidesPerView: 'auto',
        loop: false,
        spaceBetween: 50,
        navigation: {
            nextEl: '.swiper-btn-next',
            prevEl: '.swiper-btn-prev',
        },
        breakpoints: {

            480: {
                spaceBetween: 0
            }

        }
    });

    $('.icon-filter-clear').on('click', function (e) {
        e.preventDefault();
        $('form')[0].reset();
    });

    $('.hamburger').on('click', function () {
        $('.navigation').addClass('open');
        $('.overlay').css('display', 'block');
        $('body').css('overflow', 'hidden ')
    });

    $('.overlay, .close').on('click', function () {
        $('.navigation').removeClass('open');
        $('.overlay').css('display', 'none');
        $('body').css('overflow', 'auto');
    });
    $('.overlay').on('click', function () {
        $('.navigation').removeClass('open');
        $(this).fadeOut();
        $('body').css('overflow', 'auto');
        $('.category-filter').removeClass('open');
        $('.news-filter').removeClass('open');
    });

    $('.filter-toggler').on('click', function () {
        $('.category-filter').addClass('open');
        $('.overlay').fadeIn();
        $('body').css('overflow', 'hidden');
    });
    $('.category-filter .close').on('click', function () {
        $('.category-filter').removeClass('open');
        $('.overlay').fadeOut();
        $('body').css('overflow', 'auto');
    });
    $('.toggle-news-filter').on('click', function () {
        $('.news-filter').addClass('open');
        $('.overlay').fadeIn();
        $('body').css('overflow', 'hidden');
    });
    $('.news-filter .close').on('click', function () {
        $('.news-filter').removeClass('open');
        $('.overlay').fadeOut();
        $('body').css('overflow', 'auto');
    });
    $('.nice-select').niceSelect();

    $('.equipment-select').on('change', function () {
        $('.progress-bar').css('width', '50%');
        if ($(this).data('equipment') == 'business') {
            $('.business-item').fadeIn();
            $('.personal-item').hide();
        }
        if ($(this).data('equipment') == 'personal') {
            $('.personal-item').fadeIn();
            $('.business-item').hide();
        }
        $('.format-station, .type-house').prop('checked', false);
    });

    $('.format-station').on('change', function () {
        $('.progress-bar').css('width', '100%');
        $('.equipment-submit').prop('disabled', false);
    });

    $('.brand-select').on('change', function () {
        console.log($(this).val());
        if ($(this).val()) {
            $('.model-select').removeClass('disabled')
        }
    });
    $('.model-select').on('change', function () {
        if ($(this).val()) {
            $('.personal-item').removeClass('disabled');
            $('.progress-bar').css('width', '66%');
        }
    });
    $('.type-house').on('change', function () {

        $('.progress-bar').css('width', '100%');
        $('.equipment-submit').prop('disabled', false);
    });


    var slider = document.getElementById('slider');
    if ($('#slider').length != 0) {
        noUiSlider.create(slider, {
            start: [500, 10000],
            connect: true,
            tooltips: true,
            format: wNumb({
                decimals: 0
            }),
            range: {
                'min': 0,
                'max': 10000
            }
        });
    }

    var galleryTop = new Swiper('.gallery-top', {
        spaceBetween: 10,
        effect: 'fade',
        observer: true,
        observeParents: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        centeredSlides: true,
        slidesPerView: 'auto',
        // loop:true,
        touchRatio: 0.2,
        slideToClickedSlide: true,
    });
    if ($('.gallery-top').length != 0) {
        galleryTop.controller.control = galleryThumbs;
        galleryThumbs.controller.control = galleryTop;
    }
});
